/* -*- c++ -*- */
/*
 * gr-sidloc: GNU Radio OOT module for operation and experimentation with the
 * Spacecraft Identification and Localization (SIDLOC) system.
 *
 * Copyright 2022 Libre Space Foundation <https://libre.space>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "spreading_impl.h"
#include <gnuradio/io_signature.h>
#include <gnuradio/pdu.h>
#include <utility>

namespace gr {
namespace sidloc {

template <typename T>
typename spreading<T>::sptr spreading<T>::make(code::sptr c,
                                               T scale,
                                               size_t repetitions,
                                               bool channel_interleaving,
                                               const std::string& length_tag_key,
                                               const std::string& period_tag_key)
{
    return gnuradio::make_block_sptr<spreading_impl<T>>(
        c, scale, repetitions, channel_interleaving, length_tag_key, period_tag_key);
}

spreading<uint8_t>::sptr spreading<uint8_t>::make(code::sptr c,
                                                  size_t repetitions,
                                                  bool channel_interleaving,
                                                  const std::string& length_tag_key,
                                                  const std::string& period_tag_key)
{
    return gnuradio::make_block_sptr<spreading_impl<uint8_t>>(
        c, 1, repetitions, channel_interleaving, length_tag_key, period_tag_key);
}


/*
 * The private constructor
 */
template <typename T>
spreading_impl<T>::spreading_impl(code::sptr c,
                                  T scale,
                                  size_t repetitions,
                                  bool channel_interleaving,
                                  const std::string& length_tag_key,
                                  const std::string& period_tag_key)
    : gr::tagged_stream_block(
          "spreading",
          gr::io_signature::make(0, 0, 0),
          gr::io_signature::make(channel_interleaving ? 1 : c->channels(),
                                 channel_interleaving ? 1 : c->channels(),
                                 sizeof(T)),
          /* Happy hacky re-purposing of the tagged_stream_block */
          period_tag_key),
      m_interleave(channel_interleaving),
      m_scale(scale),
      m_repetitions(repetitions),
      m_length_key(length_tag_key),
      m_code(c),
      m_produced(0),
      m_remaining(0),
      m_cnt(0)

{
    this->message_port_register_in(msgport_names::pdus());
}

/*
 * Our virtual destructor.
 */
template <typename T>
spreading_impl<T>::~spreading_impl()
{
}

template <typename T>
T spreading_impl<T>::scale_bit(bool b)
{
    /* Cast it to integer so we can balance it around 0 */
    ssize_t x = b;
    x = 2 * x - 1;
    return x * m_scale;
}

template <>
gr_complex spreading_impl<gr_complex>::scale_bit(bool b)
{
    /* Cast it to integer so we can balance it around 0 */
    gr_complex x(b ? 1.0f : -1.0f, 0);
    return x * m_scale;
}

template <>
uint8_t spreading_impl<uint8_t>::scale_bit(bool b)
{
    return b;
}

template <typename T>
int spreading_impl<T>::calculate_output_stream_length(const gr_vector_int& ninput_items)
{
    if (m_remaining == 0) {
        pmt::pmt_t msg(this->delete_head_nowait(msgport_names::pdus()));
        if (msg.get() == NULL) {
            return 0;
        }

        if (!pmt::is_pair(msg)) {
            throw std::runtime_error(this->alias() + " received a malformed pdu message");
        }

        m_meta = pmt::car(msg);
        m_data = pmt::cdr(msg);
        m_remaining = pmt::blob_length(m_data);
        m_produced = 0;
        m_cnt = 0;
        m_code->reset();
    }

    /*
     * Spreading can be quite long the scheduler buffering may not have enough output
     * items. So in order to deal with this issue and simplify the logic of the work() we
     * operate in a per-byte basis
     */
    return static_cast<int>(m_interleave ? 8 * m_code->length() * m_code->channels()
                                         : 8 * m_code->length());
}


template <typename T>
void spreading_impl<T>::spread_byte(gr_vector_void_star& output_items, uint8_t b)
{
    std::vector<T*> channels;
    /*
     * NOTE: due to possible user selectable interleaving,
     * we need the actual channels not the code channels
     */
    for (auto i : output_items) {
        channels.push_back(static_cast<T*>(i));
    }

    std::bitset<8> bits(b);
    for (size_t i = 0; i < 8; i++) {
        spread_bit(channels, bits[7 - i]);
    }
}

template <typename T>
void spreading_impl<T>::spread_bit(std::vector<T*>& out, bool b)
{
    std::vector<bool> x(m_code->channels());
    if (m_interleave) {
        for (size_t i = 0; i < m_code->length(); i++) {
            m_code->next(x);
            for (size_t j = 0; j < m_code->channels(); j++) {
                *out[0]++ = scale_bit(x[j] ^ b);
            }
        }
    } else {
        for (size_t i = 0; i < m_code->length(); i++) {
            m_code->next(x);
            for (size_t j = 0; j < m_code->channels(); j++) {
                *out[j]++ = scale_bit(x[j] ^ b);
            }
        }
    }
}

template <typename T>
int spreading_impl<T>::work(int noutput_items,
                            gr_vector_int& ninput_items,
                            gr_vector_const_void_star& input_items,
                            gr_vector_void_star& output_items)
{
    if (m_remaining == 0) {
        return 0;
    }

    /* If we are dealing with the start of the frame, propagate also the metadata */
    if (m_produced == 0) {
        if (!pmt::eq(m_meta, pmt::PMT_NIL)) {
            pmt::pmt_t klist(pmt::dict_keys(m_meta));
            for (size_t i = 0; i < pmt::length(klist); i++) {
                pmt::pmt_t k(pmt::nth(i, klist));
                pmt::pmt_t v(pmt::dict_ref(m_meta, k, pmt::PMT_NIL));
                for (size_t j = 0; j < output_items.size(); j++) {
                    this->add_item_tag(
                        j, this->nitems_written(j), k, v, this->alias_pmt());
                }
            }
        }
        for (size_t j = 0; j < output_items.size(); j++) {
            this->add_item_tag(j,
                               this->nitems_written(j),
                               pmt::mp(m_length_key),
                               pmt::from_uint64(m_repetitions * m_code->length() * 8),
                               this->alias_pmt());
        }
    }

    size_t len(0);
    const uint8_t* d = (const uint8_t*)uniform_vector_elements(m_data, len);
    spread_byte(output_items, d[m_produced]);
    m_cnt++;
    if (m_cnt == m_repetitions) {
        m_produced++;
        m_remaining--;
        m_cnt = 0;
    }


    return static_cast<int>(m_interleave ? 8 * m_code->length() * m_code->channels()
                                         : 8 * m_code->length());
}

template class spreading<float>;
template class spreading<gr_complex>;
template class spreading<uint8_t>;

} /* namespace sidloc */
} /* namespace gr */
